# FLISoL Fusa

Sitios web históricos del FLISoL Fusagasugá (Colombia).

Migrados a html estático usando HTTRack (https://www.httrack.com/).

# Tecnologías originales

FLISoL Fusa 2015 - Joomla! 2.5

FLISoL Fusa 2016 - Wordpress

FLISoL Fusa 2021 en adelante - HTML+CSS+JS 